Source: irqbalance
Section: utils
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Uploaders:
 Anibal Monsalve Salazar <anibal@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-exec,
 dpkg-dev (>= 1.16.1~),
 libcap-ng-dev [linux-any],
 libglib2.0-dev (>= 2.28),
 libncurses5-dev,
 libnuma-dev [!hurd-any !kfreebsd-any !armel !armhf],
 libsystemd-dev [linux-any],
 pkg-config,
 xutils-dev,
 dh-runit (>= 2.8.3),
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/debian/irqbalance.git
Vcs-Browser: https://salsa.debian.org/debian/irqbalance
Homepage: https://github.com/Irqbalance/irqbalance

Package: irqbalance
Architecture: linux-any
Depends:
 lsb-base (>= 3.1),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks: ${runit:Breaks}
Description: Daemon to balance interrupts for SMP systems
 Daemon to balance interrupts across multiple CPUs, which can lead to
 better performance and IO balance on SMP systems. This package is
 especially useful on systems with multi-core processors, as interrupts
 will typically only be serviced by the first core.
 .
 Note: irqbalance is not useful if you don't have more than one core
       or socket.
