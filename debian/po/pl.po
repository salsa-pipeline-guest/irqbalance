# Translation of irqbalance debconf templates to Polish.
# Copyright (C) 2009
# This file is distributed under the same license as the irqbalance package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: irqbalance@packages.debian.org\n"
"POT-Creation-Date: 2009-10-17 22:41+1100\n"
"PO-Revision-Date: 2012-03-11 12:19+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Type: boolean
#. Description
#: ../irqbalance.templates:1001
msgid "Would you like to enable irqbalance?"
msgstr "Włączyć irqbalance?"

#. Type: boolean
#. Description
#: ../irqbalance.templates:1001
msgid ""
"Enable the irqbalance daemon to balance IRQs on SMP systems and systems with "
"hyperthreading."
msgstr ""
"Debmon irqbalance rozkłada obciążenie IRQ na komputerach wyposażonych w SMP "
"i hyperthreading."

#. Type: boolean
#. Description
#: ../irqbalance.templates:2001
msgid "Would you like to balance the IRQs once?"
msgstr "Rozłożyć obciążenie IRQ jednorazowo?"

#. Type: boolean
#. Description
#: ../irqbalance.templates:2001
msgid ""
"irqbalance can run in one shot mode, where the IRQs are balanced only once. "
"This is advantageous on hyperthreading systems such as the Pentium 4, which "
"appear to be SMP systems, but are really one physical CPU."
msgstr ""
"Program irqbalance może zostać uruchomiony tylko jeden raz, aby jednorazowo "
"rozłożyć obciążenie IRQ. Jest to korzystne na komputerach z procesorem "
"hyperthreading takim jak Pentium 4, który wygląda na SMP, ale fizycznie jest "
"pojedynczym procesorem."
